

  class  Vehicle {
    constructor(brand,yearManufactured){
        this.brand = brand;
        this.yearManufactured = yearManufactured;
    }
    print() {
        return  "Thương hiệu: " + this.brand + "  " +  "Năm sản xuất: " + this.yearManufactured;
    }
  
}
export {Vehicle}