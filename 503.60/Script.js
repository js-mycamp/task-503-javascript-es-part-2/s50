  // import Vehicle 
  import { Vehicle  } from './vehicle.js';
  // import Car 
  import {Car} from './car.js';
  // import Motorbike 
  import {Motorbike} from './motobike.js';
  
  
      let vehicle1 = new Vehicle("Honda",2023);
      console.log("Thương hiệu vehicle1: " + vehicle1.brand);
      console.log("Năm SX  vehicle1: " + vehicle1.yearManufactured);
      console.log("Print vehicle1:"  + vehicle1.print());
  
     
  
      let car1 = new Car ("honda",1998,2,"JS30")
      console.log("Honk car1: " + car1.honk())
      console.log("Print car1: " + car1.print())
  
  
    
  
      let moto1 = new Motorbike ("Suzuki",2023,461654,"SUZU998")
      console.log("Honk moto1: " + moto1.honk())
  
      console.log(moto1 instanceof Car)