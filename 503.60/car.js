import { Vehicle } from './vehicle.js';
class Car extends Vehicle {
    constructor(brand, yearManufactured, vId, modelName) {
        super(brand, yearManufactured)
        //  super(vId,modelName)
        this.vId = vId;
        this.modelName = modelName;
    }
    honk() {
        return "vID: " + this.vId + " - " + "modelName: " + this.modelName;
    }

}
export { Car }